#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Script for periodic readings of data from devices
# connected through serial ports. Suited for Win OS and python2.6 and
# higher (not for python 3.0).
# (C) 2011 Ales Bublik <ales.bublik@gmail.com>

import minimalmodbus
import sched
import os
import os.path
import codecs
import time
import scanwin32
import serial
import subprocess
import sys
import re
import ConfigParser
from ConfigParser import SafeConfigParser

FILE_NAME = 'mereni.txt'
TEMPLATE_HEADER = "Rok,mes,den,hod,min,s"
INITIAL_FILE = 'conf.ini'
INITIAL_SECTION = 'initial_values'
CHANGED_INITIAL_VALUES = {}
SENSORS_COUNT = 0

def default_input(key, prompt_string, default=''):
    initial_value = get_initial_value(key)
    if initial_value or default:
        text = initial_value and initial_value or default
        extra_prompt = "(prednastaveno: %s)" % text
    else:
        extra_prompt = ''
    value = raw_input(prompt_string % extra_prompt)
    return value and value or initial_value

def main():
    initialize_configuration()
    try_serial()

    with_digitemp = default_input('measure_digitemp', "Snimat teplotni cidla 'ano/ne', %s: ", default="ano")
    CHANGED_INITIAL_VALUES['measure_digitemp'] = with_digitemp
    if with_digitemp in ('ano', 'a', 'y'):
        terms_template = "\t" + probe_temperature_ports()
        CHANGED_INITIAL_VALUES['measure_digitemp'] = 'ano'
    else:
        terms_template = ""
        CHANGED_INITIAL_VALUES['temperature_port'] = get_initial_value('temperature_port')

    regulator_template = ""
    with_regulator = default_input("with_regulator", "Snimat regulator 'ano/ne' %s: ", default="ano") or None
    if with_regulator in ('ano', 'a', 'y') or with_regulator is None:
        CHANGED_INITIAL_VALUES['with_regulator'] = with_regulator and with_regulator or "ano"
        regulator_degree = default_input("regulator_degree", "Na jakou teplotu chcete regulovat %s: ", default="30") or None
        if regulator_degree is None or float(regulator_degree) > 0:
            CHANGED_INITIAL_VALUES['regulator_degree'] = regulator_degree and float(regulator_degree) or 30
            regulator_template = "\takt_tep\tpoz_tep"
        else:
            sys.exit(1)

        regulator_port = default_input("regulator_port", "Na jaky port je pripojen regulator teploty %s: ", default='8') or None
        if regulator_port or regulator_port is None:
            regulator_port = regulator_port and int(regulator_port) or 8
            initialize_regulator(regulator_port, float(regulator_degree))
            CHANGED_INITIAL_VALUES['regulator_port'] = regulator_port
        else:
            CHANGED_INITIAL_VALUES['with_regulator'] = 'ne'

    ser_list, template_serials = specify_ports()
    f = file_creation()
    file_description = raw_input("Popis mereni: ") or None
    measure_delay = int(default_input('interval', "Interval mezi jednotlivym merenim v sekundach %s: ", default='min 10 sekund'))
    CHANGED_INITIAL_VALUES['interval'] = measure_delay
    f.write("%s\r\n" % file_description)
    f.write("%s\tcas (s)\t%s%s%s\r\n" % (TEMPLATE_HEADER, template_serials, terms_template, regulator_template))
    loop(ser_list, measure_delay, f)
    finish(f, ser_list)

def initialize_regulator(port, degree):
    instrument = minimalmodbus.Instrument("COM%d" % port, 1)
    instrument.serial.baudrate = 9600
    instrument.serial.timeout = 0.1
    instrument.write_register(100, degree, 1, 6)

def read_from_regulator(register):
    if 'regulator_port' not in CHANGED_INITIAL_VALUES:
        return
    port = CHANGED_INITIAL_VALUES['regulator_port']
    instrument = minimalmodbus.Instrument("COM%d" % port, 1)
    instrument.serial.baudrate = 9600
    instrument.serial.timeout = 0.1
    return str(instrument.read_register(register, 1)).replace('.', ',')

def initialize_configuration():
    parser = SafeConfigParser()
    if not os.path.exists(INITIAL_FILE):
        f = codecs.open(INITIAL_FILE, encoding="utf-8", mode="w")
        parser.add_section(INITIAL_SECTION)
        parser.write(f)
        f.close()

def get_initial_value(key):
    item = None
    with codecs.open(INITIAL_FILE, encoding="utf-8", mode="r") as f:
        parser = SafeConfigParser()
        parser.readfp(f)
        try:
            item = parser.get(INITIAL_SECTION, key)
        except ConfigParser.NoOptionError, e:
            pass
    return item

def probe_temperature_ports():
    try:
        port_number = int(default_input('temperature_port', "Zvolte cislo portu pro mereni teploty %s: ", default="napr. 1"))
        port_number -= 1
    except ValueError, e:
        print("Zadane cislo neni cislem!")
        sys.exit(1)
    CHANGED_INITIAL_VALUES['temperature_port'] = port_number + 1
    port_spec = "/dev/ttyS%s" % port_number
    print("Probiha inicializace teplotnich cidel cca 60 sekund ...")
    digitemp_proc = subprocess.Popen(['digitemp_DS9097.exe', '-i', '-s', port_spec],
		    shell=True, stdout=subprocess.PIPE)
    result = digitemp_proc.communicate()
    result = result[0].split("\n")
    result = [int(line.split(" ", 2)[1].strip("#")) + 1 for line in result if line.startswith("ROM #")]
    print("Nactene cidla: %s" % (", ".join([str(i) for i in result]),))
    result = "\t".join(["Tep%s" % i for i in result])
    global SENSORS_COUNT
    with open('.digitemprc', mode="r") as f:
        for line in f:
            if line.startswith("SENSORS "):
                SENSORS_COUNT = int(line.strip().split()[1])
    if SENSORS_COUNT == 0:
        print("Nelze nacist sensory.")
        sys.exit()
    return result


def specify_ports():
    """
    Vyzve uzivatele, aby zadal cisla portu, ze kterych se budou odecitat udaje.
    Vraci seznam seriovych portu spolus se sablonou, ktera urcuje format sloupcu
    pro zaznam do souboru.
    """
    ser_list = default_input("port_list", "Zadej cisla portu, ktera budou pouzita %s: ", default="1,2,3,4,5,6,7,8")
    if ser_list == "":
        print("Nezadal jste cislo portu!")
        sys.exit(1)
    CHANGED_INITIAL_VALUES['port_list'] = ser_list
    if ',' not in ser_list:
        ser_list = [int(ser_list.strip()) - 1]
        template = "COM%d" % (ser_list[0] + 1)

    else:
        ser_list = [int(s.strip()) - 1 for s in ser_list.split(",")]
        template = "\t".join(["COM%d" % (s + 1) for s in ser_list])
    return (ser_list, template)

def try_serial():
    """
    Pomocna funkce vyuziva modulu scanwin32, ktery na pocitaci vyhleda vsechny
    dostupne seriove porty.
    """
    ser_list = []
    for order, port, desc, hwid in sorted(scanwin32.comports()):
        print "%-10s: %s (%s) ->" % (port, desc, hwid),
        try:
            ser = serial.Serial(port)
        except serial.serialutil.SerialException:
            print("Nelze otevrit!")
        else:
            print("Pripraven")
            ser_list.append(ser)
    return ser_list[:]

def ensure_path(path):
    try:
        os.makedirs(path)
    except OSError, e:
        if os.path.isdir(path):
            pass
        else:
            print(e)
            sys.exit()

def file_creation():
    """
    Funkce testuje, zda predany nazev souboru lze pouzit pro vytvoreni noveho
    souboru. Tzn. pokud existuje v adresari soubor se stejnym nazvem, pak program
    skonci.
    """
    output_path = get_initial_value('output_path')
    if output_path is None:
        output_path = "c:"
    CHANGED_INITIAL_VALUES['output_path'] = output_path
    ensure_path(output_path)
    get_filename = lambda : raw_input("Nazev souboru (%s): " % FILE_NAME) or FILE_NAME
    filename = get_filename()
    if output_path:
        filename = os.path.join(output_path, filename)
    while os.path.exists(filename):
        print("Soubor jiz existuje, zvolte jiny!")
        filename = get_filename()
        if output_path:
            filename = os.path.join(output_path, filename)
    try:
        f = codecs.open(filename, encoding="utf-8", mode="w")
    except (IOError, ValueError):
        print("Problem s vytvorenim souboru!")
    return f

def setup_serials(serial_list):
    """
    Provede na vsech seriovych portech z serial_list inicializaci kanalu a uvede
    do stavu, kdy lze jiz vysilat pozadavky.
    """
    def prepare_channel(s):
        ser = serial.Serial(s, timeout=1)
        if ser.isOpen():
            print "Port COM%d otevren." % (ser.port + 1)
            ser.read(ser.inWaiting())
        else:
            print "Port COM%d se nepodarilo otevrit." % (ser.port + 1)
            exit()
        ser.write("E1\r")
        time.sleep(1)
        return ser
    return [prepare_channel(s) for s in serial_list]

def correct_left_zero(ret):
    "Pomocna funkce pro osetreni formatu vraceneho cisla. Tjz. odrizne prebytecne nuly."
    ret = ret.lstrip('0')
    if ret[0] == '.':
        ret = "0%s" % ret
    return ret

def get_data(sch, serial_list, storage, now, delay, counter):
    """
    Funkce se stara o nacitani dat ze seriovych portu a take obstarava spusteni
    sebe samotne v nasledujicim cyklu. Odchyceni vyjimek zajistime uzavreni
    seriovych portu a souboru.
    """
    def read_res(ser):
        res = ser.read(ser.inWaiting()).strip()
        final_res = ""
        if res is None:
            return final_res
        if "+" in res:
            final_res = "+%s" % correct_left_zero(res.split("+")[1])
        elif "-" in res:
            final_res = "-%s" % correct_left_zero(res.split("-")[1])
        else:
            final_res = res
        return final_res.replace(".", ",")
    try:
        [s.write("1\r") for s in serial_list]
        # next scheduled reading
        next_time = now + delay
        next_counter = counter + delay
        sch.enterabs(next_time, 1, get_data,
                           (sch, serial_list, storage, next_time, delay, next_counter))
        ####
        final_results = []
        if CHANGED_INITIAL_VALUES['measure_digitemp'] == 'ano':
            output = subprocess.Popen(['digitemp_DS9097.exe', '-a'], shell=True, stdout=subprocess.PIPE).communicate()
            temp_results = {}
            output = re.split(r'(Sensor \d C: \d+.\d+)', output[0])
            for line in output[1:]:
                if 'Sensor' in line:
                    number = line.strip().split()[1]
                    degree_celsius = line.strip().split()[-1].replace('.', ',')
                    temp_results[number] = degree_celsius

            for i in range(SENSORS_COUNT):
                final_results.append(temp_results.get(str(i), " "))

        if 'with_regulator' in CHANGED_INITIAL_VALUES and CHANGED_INITIAL_VALUES['with_regulator'] == 'ano':
            actual_degree = read_from_regulator(20)
            initial_degree = read_from_regulator(21)
            regulator_results = [actual_degree, initial_degree]
        else:
            regulator_results = []

            time.sleep(1)
            results = [read_res(s) for s in serial_list]
            t = time.gmtime(next_time)
            t = "%d,%d,%d,%d,%d,%d" % (t.tm_year, t.tm_mon, t.tm_mday, t.tm_hour,
                                       t.tm_min, t.tm_sec)
            results2 = "\t".join(["%s" % r for r in results + final_results + regulator_results])
            store = "%s\t%d\t%s\r\n" % (t, counter, results2)
            print store.replace('\t', "  ").replace('\r\n', '')
            storage.write(store)
            storage.flush()
            os.fsync(storage)

    except (IOError, KeyboardInterrupt, SystemExit):
        finish(storage, serial_list)
        print("Konec cteni udaju.\n")
        sys.exit(1)

def loop(serial_list, delay, storage):
    "Hlavni smycka, ktera uvede nacitani udaju do pohybu."
    try:
        serial_list2 = setup_serials(serial_list)
        # Pred nacitanim hodnot vyprazdnime buffer
        [s.read(s.inWaiting()) for s in serial_list2]
        sch = sched.scheduler(time.time, time.sleep)
        print("Stiskem klavesy Ctrl-C ukoncite program.")
        raw_input("Stiskem klavesy Enter zacne nacitani hodnot: ")
        print("Probiha nacitani dat.")
        counter = 0
        now = time.time()
        sch.enterabs(now, 1, get_data,
                (sch, serial_list2, storage, now, delay, counter))
        sch.run()
    except (IOError, KeyboardInterrupt, SystemExit):
        finish(storage, serial_list2)
        print("Konec cteni udaju.\n")
        sys.exit(1)

def finish(f, serial_list):
    "Uzavre soubor a seriove porty."
    try:
        f.close()
        [s.close for s in serial_list]
    except Exception, e:
        print(e)
        print("Nepodarilo se korektne zavrit soubor nebo seriovy port!")

    with codecs.open(INITIAL_FILE, encoding="utf-8", mode="w") as f:
        parser = SafeConfigParser()
        parser.add_section(INITIAL_SECTION)
        for key in CHANGED_INITIAL_VALUES:
            parser.set(INITIAL_SECTION, key, str(CHANGED_INITIAL_VALUES[key]))
        parser.write(f)


if __name__ == '__main__':
    """Jako prvni funkce, ktera se spusti po lpusteni skriptu, bude main(). """
    main()
